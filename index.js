const express = require("express");
const querystring = require("querystring");

const app = express();
app.use(express.json());
const port = process.env.PORT || 65385;

app.listen(port, () => {
  console.log("Server is up on port: " + port);
});

app.get("/home/callback", function (req, res) {
  const queryString = querystring.stringify(req.query);
  const proxyBaseUrl = `http://api.dokker.test:9280/connections/callback?${queryString}`;
  //  const proxyBaseUrl = `http://api-dev.dokker.co/connections/callback?${queryString}`;
  res.redirect(proxyBaseUrl);
});

app.post("/notify", function (req, res) {
  payload = {
    name: "Jimbo Cortes",
  };

  console.log(payload);
  res.send(payload);
});

app.get("/api/tenancies", function (req, res) {
  res.send([
    {
      payer_unique_id: "TEN00014",
    },
  ]);
});

app.post("/chrome_extension_api/v1/callback", function (req, res) {
  const isEven = () => {
    const items = [1, 2, 4, 6];
    const index = Math.floor(Math.random() * 100) % items.length;
    const value = items[index];
    return value % 2 === 0;
  };

  if (req.body.kind === "UpdateReferenceId") {
    if (isEven()) {
      const success = {
        message: "Success",
      };

      res.send(success);
    } else {
      const error = {
        error: {
          reference_id: ["does not exist."],
        },
      };

      res.status(422).send(error);
    }
  } else {
    res.status(500).end();
  }
});

app.get("/");

// {
//     "kind": "UpdateReferenceId",
//     "data": {
//         "reference_id": "1122341d3",
//         "pms_url": "https://app.propertyme.com/#/property/card/ab7502a7-3a22-4300-8e89-d5a724a332ef",
//         "bank_reference": "12121, 1122341d3",
//         "lease": {
//             "uuid": "20d6ddce-43b1-44a6-8b13-a9e522ddc17c"
//         }
//     }
// }

// {
//   "message": "Success"
// }

// {
//   "error": {
//     "reference_id": [
//       "does not exist."
//     ]
//   }
// }
